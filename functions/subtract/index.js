'use strict';

console.log('start subtract');

exports.subtract = (e, ctx) => {
    ctx.succeed(Number(e.a) - Number(e.b));
};
