'use strict';

const eslint = require('gulp-eslint'),
    gulp     = require('gulp'),
    istanbul = require('gulp-istanbul'),
    mocha    = require('gulp-mocha'),
    rules    = require('@edjboston/eslint-rules'),
    sequence = require('gulp-sequence');

// Lint all JS files (including this one)
gulp.task('lint', () => {
    const globs = [
        'lib/*.js',
        'test/*.js',
        'functions/*/*.js',
        'gulpfile.js',
        '!node_modules/**'
    ];

    return gulp.src(globs)
        .pipe(eslint({
            extends       : 'eslint:recommended',
            parserOptions : { ecmaVersion : 6 },
            rules
        }))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

// Instrument the code
gulp.task('cover', () => {
    const globs = [
        'functions/*/*.js',
        'lib/*.js'
    ];

    return gulp.src(globs)
        .pipe(istanbul())
        .pipe(istanbul.hookRequire());
});

// Run the tests
gulp.task('test', () => {
    return gulp.src('test/*.js')
        .pipe(mocha({
            require : [ 'should' ]
        }))
        .pipe(istanbul.writeReports())
        .pipe(istanbul.enforceThresholds({
            thresholds : { global : 100 }
        }));
});

// Helper task for GitLab CI
gulp.task('gitlab', sequence('lint', 'cover', 'test'));
