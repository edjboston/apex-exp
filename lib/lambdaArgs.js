'use strict';

exports.lambdaArgs = (a, b, callback) => {
    const e = { a, b };

    const ctx = {
        succeed : callback
    };

    return [ e, ctx ];
};
