'use strict';

const add      = require('../functions/add').add,
    lambdaArgs = require('../lib/lambdaArgs').lambdaArgs;

describe('add', () => {
    it('should add 1 and 1 correctly', done => {
        add(...lambdaArgs(1, 1, result => {
            result.should.eql(2);
            done();
        }));
    });

    it('should add -1 and -1 correctly', done => {
        add(...lambdaArgs(-1, -1, result => {
            result.should.eql(-2);
            done();
        }));
    });

    it('should add -1 and 1 correctly', done => {
        add(...lambdaArgs(-1, 1, result => {
            result.should.eql(0);
            done();
        }));
    });
});
