'use strict';

const lambdaArgs = require('../lib/lambdaArgs').lambdaArgs,
    subtract     = require('../functions/subtract').subtract;

describe('subtract', () => {
    it('should subtract 1 from 1 correctly', done => {
        subtract(...lambdaArgs(1, 1, result => {
            result.should.eql(0);
            done();
        }));
    });

    it('should subtract 1 from -1 correctly', done => {
        subtract(...lambdaArgs(-1, 1, result => {
            result.should.eql(-2);
            done();
        }));
    });

    it('should subtract -1 from -1 correctly', done => {
        subtract(...lambdaArgs(-1, -1, result => {
            result.should.eql(0);
            done();
        }));
    });
});
